import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.Node;
import javafx.scene.Group;

/**
 * This class represents wallpaper for the game with a white delimiter between the left an the right side.
 */
public class Wallpaper implements Displayable {
    private Group group;


    /**
     * This constructor generates a wallpaper from two parameters.
     * @param _width the  the game's field's width.
     * @param _height the  the game's field's height.
     */
    public Wallpaper(double _width, double _height) {
        Rectangle rectangle = new Rectangle();
        rectangle.setX(0);
        rectangle.setY(0);
        rectangle.setWidth(_width);
        rectangle.setHeight(_height);
        rectangle.setFill(Color.BLACK);

        Rectangle line = new Rectangle();
        line.setX(_width/2 -1);
        line.setY(0);
        line.setWidth(2.);
        line.setHeight(_height);
        line.setFill(Color.WHITE);

        group = new Group();
        group.getChildren().addAll(rectangle, line);
    }

    /**
     * Returns the drawable JavaFX node which represents the wallpaper's shape.
     * @return a JavaFX node representing the wallpaper.
     */
    public Node display(){
        return group;
    }

}
