import javafx.scene.input.MouseEvent;

/**
 * This class extends AbstractRacket and represents a Racket which is controlled by the mouse.
 */
public class MouseControlledRacket extends AbstractRacket{

	/**
	 * The constructor builds a player racket from two parameters.
	 * @param _fieldWidth This parameter is the width of the game field.
	 * @param _fieldHeight This parameter is the height of the game field.
	 */
	public MouseControlledRacket(double _fieldWidth, double _fieldHeight){
		super(_fieldWidth, _fieldHeight);

	}

	/**
	 * This method lets the racket react from a mouse event by moving to the mouse's position.
	 * @param e The Mouse event.
	 */
	@Override
	public void reactToMouse(MouseEvent e){
		figure.setY(computesLegalYPosition(e.getY() - racketHeight/2));

	}
}
