import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.scene.Node;


/**
 * This class represents a counter for the game.
 * It stores the score of each player and allows to increment the score.
 * It also implements a text JavaFX node which shows the score.
 */
public class Counter implements  Displayable{

    private Text text;
    private int leftScore; //The left player's score.
    private int rightScore; // The right player's score.

    /**
     * This constructor generates a Counter object.
     * @param _fieldWidth The field's width. It is needed to center the text.
     */
    public Counter(double _fieldWidth){
        //The two scores are set to zero.
        leftScore = 0;
        rightScore = 0;

        text = new Text(0, 40, "");
        text.setFill(Color.WHITE);
        text.setFont(Font.font("Verdana", FontWeight.BOLD, 30));

        //centers the text
        text.setTextAlignment(TextAlignment.CENTER);
        text.setWrappingWidth(_fieldWidth);

        //Computes the string to show based on scores.
        refreshText();


    }

    /**
     * Computes the string to show based on scores.
     */
    private void refreshText(){
        text.setText(leftScore + "   " + rightScore);
    }

    /**
     * Returns the right player's score.
     * @return the right player's score.
     */
    public int getRightScore(){
        return rightScore;
    }

    /**
     * Returns the left player's score.
     * @return the left player's score.
     */
    public int getLeftScore(){
        return leftScore;
    }

    /**
     * Increments the right player's score.
     */
    public void incrRightScore(){
        rightScore += 1;
        refreshText();
    }

    /**
     * Increments the left player's score.
     */
    public void incrLeftScore(){
        leftScore += 1;
        refreshText();
    }

    /**
     * Returns the drawable JavaFX node which represents the counter's shape.
     * @return a JavaFX node representing the counter.
     */
    public Node display(){
        return text;
    }

}
