import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;

import java.util.Random;

/**
 * This class represents the game's environment and embeds each components.
 */
public class Field implements Displayable{
	private Ball ball; //the ball
	private AbstractRacket rightRacket; //the right player
	private AbstractRacket leftRacket; //the left player
	private Counter counter; //the score counter
	private double width; //the field's width
	private double height; //the field's height

	private Group rootNode;

	private double initialSpeedFactor;
	private double speedFactor;
	private double speedIncrement;

	private double leftLimit;
	private double rightLimit;
	private double topLimit;
	private double bottomLimit;

    /**
     * This constructor builds a field from several parameters.
     * @param _leftPlayer This string can be "ai", "keyboard" or "mouse" according to the required kind of left player.
     * @param _rightPlayer Same that the previous one but for the right player.
     * @param _width The field's width.
     * @param _height The field's height.
     * @param _radius The ball's radius.
     */
	Field(String _leftPlayer, String _rightPlayer, double _width, double _height, double _radius){
		ball = new Ball(_width/2, _height/2, _radius);

		//The left player is built
		if(_leftPlayer.equals("keyboard")){
			KeyboardControlledRacket _leftRacket= new KeyboardControlledRacket(_width,  _height);
			_leftRacket.setKeyBinding(KeyCode.Z, KeyCode.S);
			leftRacket = _leftRacket;
		}
		else if (_leftPlayer.equals("mouse")) {
			leftRacket = new MouseControlledRacket(_width, _height);
		}
		else
		leftRacket= new AIRacket(_width, _height);
		leftRacket.setSide("left");

    //The right player is built.
		if(_rightPlayer.equals("keyboard"))
		rightRacket = new KeyboardControlledRacket(_width, _height);
		else if (_rightPlayer.equals("mouse"))
		rightRacket = new MouseControlledRacket(_width, _height);
		else
		rightRacket = new AIRacket(_width, _height);
		rightRacket.setSide("right");


		width = _width;
		height = _height;
		rootNode = new Group();

		//the counter and the wallpaper are built.
		counter = new Counter(width);
		Wallpaper wallpaper = new Wallpaper(width, height);

		//Sets the initial speed factor and the increment.
		initialSpeedFactor = 1000;
		speedIncrement = 100;
		speedFactor = initialSpeedFactor;

		//Adds each drawable JavaFX Node to a group
		rootNode.getChildren().addAll(
		wallpaper.display(),
		ball.display(),
		leftRacket.display(),
		rightRacket.display(),
		counter.display()
		);

		//Sets the area where the center of the ball can move.
		leftLimit = leftRacket.getWidth() + leftRacket.getScreenSpace() + ball.getRadius();
		rightLimit = _width - rightRacket.getWidth() - rightRacket.getScreenSpace() -  ball.getRadius();
		topLimit = ball.getRadius();
		bottomLimit = _height - ball.getRadius();
	}

    /**
     * getS the drawable JavaFX node which represents the shape of the field.
     * @return a JavaFX node representing the field.
     */
	public Group display(){
		return rootNode;
	}

    /**
     * Centers the ball.
     */
	public void centersBall(){
		ball.display().setCenterX(width/2);
		ball.display().setCenterY(height/2);
	}

    /**
     * Increases the ball's speed factor.
     */
	public void increasesSpeedFactor(){
		speedFactor += speedIncrement;
	}

    /**
     * Reset the ball's speed factor to the initial one.
     */
	public void resetSpeedFactor(){
		speedFactor = initialSpeedFactor;
	}


    /**
     * Computes a new random speed direction for the ball.
     */
	public void setRandomSpeed(){
		Random rand = new Random();
		double sX = 0;
		double sY = 0;

		//sX*sY < 0.4 makes sure that the speed direction is interesting
        //We will discuss that in the report.
		while(sX*sY < 0.4) {
			double angle = rand.nextDouble() * 2 * Math.PI;
			sX = Math.cos(angle);
			sY = Math.sin(angle);
		}
		ball.setSpeed(new Vector(sX, sY));

	}

    /**
     * getS the ball.
     * @return the ball object.
     */
	public Ball getBall(){
		return ball;
	}

    /**
     * getS the left racket.
     * @return The left racket.
     */
	public AbstractRacket getLeftRacket(){
		return leftRacket;
	}

    /**
     * getS the counter.
     * @return The counter object.
     */
	public Counter getCounter(){
		return counter;
	}

    /**
     * getS the right racket.
     * @return The right racket.
     */
	public AbstractRacket getRightRacket(){
		return rightRacket;
	}

    /**
     * Makes the ball bounce.
     * @param hit The hit generated from the collision.
     */
	public void bouncesBall(Hit hit){

		Vector normal = hit.toNormal();
		computeNewSpeed(normal);
	}

    /**
     * Computes the next KeyFrame for the next animation when the ball bounces.
     * @param currentTime The current time of the animation.
     * @return A KeyFrame to add in the Engine's timeline.
     */
	public KeyFrame computesBallFrame(Duration currentTime){
		Vector speed = ball.getSpeed();

		//Computes the time required by the ball to hit the next obstacle.
		double time = computeTrajectTime();

		//Computes the ball's position at the next collision.
		Vector currentPosition = ball.getPosition();
		double nx = currentPosition.getX() + time*speed.getX()*speedFactor;
		double ny = currentPosition.getY() + time*speed.getY()*speedFactor;

		//Sends information to the rackets, so that the AI can react.
		if(speed.getX() > 0){

			rightRacket.receivesInformation(ny);
		}
		else {
			leftRacket.receivesInformation(ny);
		}

    //Builds the KeyFrame and getS it
		Duration nextTime = currentTime.add(Duration.seconds(time));
		KeyValue nextFrameX = new KeyValue(ball.display().centerXProperty(), nx);
		KeyValue nextFrameY = new KeyValue(ball.display().centerYProperty(), ny);
		return new KeyFrame(nextTime, nextFrameX, nextFrameY);

	}

    /**
     * Computes the next KeyFrame for the next animation when the ball escapes.
     * @param currentTime The current time of the animation.
     * @return A KeyFrame to add in the Engine's timeline.
     */
	public KeyFrame computesBallExit(Duration currentTime){
		Vector speed = ball.getSpeed();

		//Computes the time needed by the ball to escape from the field.
		double time = (leftLimit + ball.getRadius()) /( Math.abs(speed.getX()) * speedFactor);

		//Computes the final position of the ball/
		Vector currentPosition = ball.getPosition();
		double nx = currentPosition.getX() + time*speed.getX()*speedFactor;
		double ny = currentPosition.getY() + time*speed.getY()*speedFactor;

        //Builds the KeyFrame and return
		Duration nextTime = currentTime.add(Duration.seconds(time));
		KeyValue nextFrameX = new KeyValue(ball.display().centerXProperty(), nx);
		KeyValue nextFrameY = new KeyValue(ball.display().centerYProperty(), ny);
		return new KeyFrame(nextTime, nextFrameX, nextFrameY);

	}


    /**
     * Computes and return a hit object to know what object the ball has hit.
     * @return a hit object.
     */
	public Hit getHit(){
		Vector currentPosition = ball.getPosition();

		if(Math.round(currentPosition.getX()) == leftLimit){
			if((Math.round(currentPosition.getY()) >
			leftRacket.getPosition().getY())
			&& (Math.round(currentPosition.getY()) <
			(leftRacket.getPosition().getY() + leftRacket.getHeight()))) {

				if(Math.round(currentPosition.getY()) == bottomLimit){
					return Hit.BOTTOM_LEFT_BOUNCE;
				}
				else if(Math.round(currentPosition.getY()) == topLimit){
					return Hit.TOP_LEFT_BOUNCE;
				}
				return Hit.LEFT_BOUNCE;
			}
			else{
				return Hit.LEFT_ESCAPE;
			}
		}
		else if(Math.round(currentPosition.getX()) == rightLimit){
			if((Math.round(currentPosition.getY()) >
			rightRacket.getPosition().getY())
			&& (Math.round(currentPosition.getY()) <
			(rightRacket.getPosition().getY() + rightRacket.getHeight()))) {

				if(Math.round(currentPosition.getY()) == bottomLimit){
					return Hit.BOTTOM_RIGHT_BOUNCE;
				}
				else if(Math.round(currentPosition.getY()) == topLimit){
					return Hit.TOP_RIGHT_BOUNCE;
				}
				return Hit.RIGHT_BOUNCE;
			}
			else{

				return Hit.RIGHT_ESCAPE;
			}
		}
		else if(Math.round(currentPosition.getY()) == bottomLimit){

			return Hit.BOTTOM_BOUNCE;
		}
		else if(Math.round(currentPosition.getY()) == topLimit){
			return Hit.TOP_BOUNCE;
		}

		//There was no bounce.
		return Hit.NO_BOUNCE;
	}

    /**
     * Computes the time needed by the ball to reach the next obstacle.
     * @return the time needed.
     */
	private double computeTrajectTime(){
		Vector currentPosition = ball.getPosition();
		Vector currentSpeed = ball.getSpeed();

		double time = 0;
		double timeX = 0;
		double timeY = 0;

		if(currentSpeed.getX() < 0){
			timeX = (leftLimit -currentPosition.getX())/(currentSpeed.getX()*speedFactor);
		}
		else{
			timeX = (rightLimit -currentPosition.getX())/(currentSpeed.getX()*speedFactor);
		}

		if(currentSpeed.getY() < 0){
			timeY = (topLimit -currentPosition.getY())/(currentSpeed.getY()*speedFactor);
		}
		else{
			timeY = (bottomLimit -currentPosition.getY())/(currentSpeed.getY()*speedFactor);
		}


		if(timeX <= timeY ) {
			time = timeX;
		}
		else{
			time = timeY;

		}
		return time;
	}

    /**
     * Computes the new ball's speed direction based on reflection's laws.
     * @param normal the normal.
     */
	private void computeNewSpeed(Vector normal){
		double normalX = normal.getX();
		double normalY = normal.getY();

		//Computes the symmetry of the speed on the normal.
		double scalarProduct = ball.getSpeed().getX()*normalX+ball.getSpeed().getY()*normalY;
		double newSpeedX = normalX*2*(-1)*scalarProduct + ball.getSpeed().getX();
		double newSpeedY = normalY*2*(-1)*scalarProduct + ball.getSpeed().getY();

		//Sets the new speed.
		ball.setSpeed(new Vector(newSpeedX, newSpeedY));
	}



}
