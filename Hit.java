/**
 * This class represents which kind of hit the ball performed.
 */
public final class Hit {

    /**
     * The ball bounced on the left player's racket.
     */
    public static final Hit LEFT_BOUNCE = new Hit(0);
    /**
     * The ball bounced on the right player's racket.
     */
    public static final Hit RIGHT_BOUNCE = new Hit(1);
    /**
     * The ball bounced on the top wall.
     */
    public static final Hit TOP_BOUNCE = new Hit(2);
    /**
     * The ball bounced on the bottom wall.
     */
    public static final Hit BOTTOM_BOUNCE = new Hit(3);
    /**
     * The ball hit the left wall but didn't hit a racket. The ball shall escape.
     */
    public static final Hit LEFT_ESCAPE = new Hit(4);
    /**
     * The ball hit the right wall but didn't hit a racket. The shall will escape.
     */
    public static final Hit RIGHT_ESCAPE = new Hit(5);
    /**
     * The ball hit the top right corner and hit a racket.
     */
    public static final Hit TOP_RIGHT_BOUNCE = new Hit(6);
    /**
     * The ball hit the top left corner and hit a racket.
     */
    public static final Hit TOP_LEFT_BOUNCE = new Hit(7);
    /**
     * The ball hit the bottom right corner and hit a racket.
     */
    public static final Hit BOTTOM_RIGHT_BOUNCE = new Hit(8);
    /**
     * The ball hit the bottom left corner and hit a racket.
     */
    public static final Hit BOTTOM_LEFT_BOUNCE = new Hit(9);
    /**
     * The ball didn't hit anything.
     */
    public static final Hit NO_BOUNCE = new Hit(10);

    private int value;

    Hit(int _value){
        value = _value;
    }

    /**
     * Computes a reflection normal vector from the kind of hit.
     * @return a Vector which represents the normal.
     */
    public Vector toNormal(){
        if (value == 0){
            return new Vector(1, 0);
        }
        else if (value == 1){

            return new Vector(-1, 0);
        }
        else if (value == 2){

            return new Vector(0, 1);
        }
        else if (value == 3){

            return new Vector(0, -1);
        }

        else if (value == 6){

            return new Vector(-1, 1);
        }

        else if (value == 7){

            return new Vector(1, 1);
        }

        else if (value == 8){

            return new Vector(-1, -1);
        }

        else if (value == 9){

            return new Vector(1, -1);
        }

        return new Vector(0, 0);

    }


}
