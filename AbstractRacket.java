import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.scene.input.KeyEvent;

/**
 * This class represents a player's racket.
 */
public abstract class AbstractRacket implements Displayable{
	protected Rectangle figure; //Contains the drawable javaFX Node
	protected static double racketWidth = 20;
  	protected static double racketHeight = 100;
	protected static double screenSpace = 20; //The space between the wall and the racket.
	protected double fieldHeight; //This information is mandatory to know which moves the racket can do.
	protected double fieldWidth; //This information is mandatory to place a right racket.

    /**
     * The constructor builds a player's racket from two parameters.
     * @param _fieldWidth This parameter is the width of the game field.
     * @param _fieldHeight This parameter is the height of the game field.
     */
	AbstractRacket(double _fieldWidth, double _fieldHeight){
		figure = new Rectangle();
		setPosition(new Vector(screenSpace, 0));
		figure.setWidth(racketWidth);
		figure.setHeight(racketHeight);
		Color color = Color.WHITE;
		figure.setFill(color); //The color of the racket is set to white.
		fieldHeight = _fieldHeight;
		fieldWidth = _fieldWidth;
	}

    /**
     *This method sets the side the rackets belongs to.
     * It changes the X position of the racket to displace it at the good place.
     * @param side A string which represents the position: "right" or "left".
     */
	public void setSide(String side){
		if(side.equals("left")){
			setPosition(new Vector(screenSpace, getPosition().getY()));
		}
		else if(side.equals("right")){
			setPosition(new Vector(fieldWidth - screenSpace - racketWidth, getPosition().getY()));
		}
	}

    /**
     * This method returns a drawable JavaFX node to show the racket.
     * @return the JavaFX Node which represents the shape of the racket.
     */
	@Override
	public Rectangle display(){
		return figure;
	}

    /**
     * Sets the position of the racket.
     * @param _position A Vector object which represents the new position of the racket.
     */
	public void setPosition(Vector _position){
		figure.setX(_position.getX());
		figure.setY(_position.getY());
	}

    /**
     * This method is called when a racket is queried to react from a keyboard event.
     * @param e The Keyboard event
     */
	public void reactToKeyboard(KeyEvent e){

	}

    /**
     * This method is called when a racket is queried to react from a mouse event.
     * @param e The Mouse event
     */
	public void reactToMouse(MouseEvent e){

	}

    /**
     * This method is called when the ball bounces and gives the information of the ball.
     * This method can be overridden to make AI see the ball.
     * @param nextYBall The Y component of the position where the ball shall hit.
     */
	public void receivesInformation(double nextYBall){

	}

    /**
     * Returns the racket's width.
     * @return the racket's width.
     */
	public double getWidth(){
		return racketWidth;
	}

    /**
     * Returns the space between the wall and the racket.
     * @return the space between the wall and the racket.
     */
	public double getScreenSpace() {
		return screenSpace;
	}

    /**
     * Returns the racket's position.
     * @return a vector which represents the racket's position..
     */
	public Vector getPosition(){
		return new Vector(figure.getX(), figure.getY());
	}

    /**
     * Returns the racket's height.
     * @return the racket's height.
     */
	public double getHeight() {
		return racketHeight;
	}

    /**
     * Computes a legal Y position form a requested Y position.
     * This method is called to be sure that the racket won't go out of the field's limits.
     * @param YRequest A requested Y position.
     * @return A proper Y position based on the request Y position.
     */
	protected double computesLegalYPosition(double YRequest){
	    //When an keyboard or a mouse event, or an AI ask to move the racket,
        //A legal position is computed from the requested position.
		return Math.max(0, Math.min(fieldHeight-getHeight(), YRequest));
	}
}
