import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


/**
 * This class extends from AbstractRacket and represents a Racket which is controlled by the keyboard.
 */
public class KeyboardControlledRacket extends AbstractRacket{


	private double racketScrollGap = 50; //the gap of the racket translation for each move.
	private KeyCode upKey; //The key which makes the racket goes up.
	private KeyCode downKey; //The key which makes the racket goes down.

	/**
	 * The constructor builds a player racket from two parameters.
	 * @param _fieldWidth This parameter is the width of the game field.
	 * @param _fieldHeight This parameter is the height of the game field.
	 */
	public KeyboardControlledRacket(double _fieldWidth, double _fieldHeight){
		super(_fieldWidth, _fieldHeight);
		//The default key binding is the following:
		upKey = KeyCode.UP;
		downKey = KeyCode.DOWN;

	}


	/**
	 * This method allows user to select the key binding to move the racket.
	 * Which two differents binding, two humans can play together.
	 * @param _upKey The key which makes the racket goes up.
	 * @param _downKey The key which makes the racket goes down.
	 */
	public void setKeyBinding(KeyCode _upKey, KeyCode _downKey){
		upKey = _upKey;
		downKey = _downKey;
	}

	/**
	 * This method allows the racket ro react from keyboard input.
	 * @param e The Keyboard event
	 */
	@Override
	public void reactToKeyboard(KeyEvent e){
		if(e.getCode() == upKey || e.getCode() == downKey){
			int factor = 1; //positive Y direction
			if(e.getCode() == upKey){
				factor = -1; //negative Y direction
			}

			double currentX = getPosition().getX();
			double currentY = getPosition().getY();
			//Computes a legal Y position from the requested Y position.
			double nextY = computesLegalYPosition(currentY + factor*racketScrollGap);
			//Sets the new position.
			setPosition(new Vector(currentX, nextY));


		}

	}


}
