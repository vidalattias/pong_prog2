import javafx.scene.shape.Circle;
import javafx.scene.paint.Color;

/**
 * This class represents the ball of the game.
 */
public class Ball implements Displayable{
	private double radius;
	private Circle figure; //The drawable JavaFX node
	private Vector speed;

	/**
	 * Returns the drawable JavaFX node which represents the graphic of the ball.
	 * @return a JavaFX node representing the ball.
	 */
	@Override
	public Circle display(){
		return figure;
	}

	/**
	 * The constructor takes three parameters.
	 * @param _x the initial X position.
	 * @param _y the initial Y position.
	 * @param _radius the ball's radius.
	 */
	public Ball(double _x, double _y, double _radius){
		radius = _radius;
		speed = new Vector(0,0);
		figure = new Circle(_x);
		figure.setCenterX(_x);
        figure.setCenterY(_y);
		Color color = Color.WHITE;
		figure.setFill(color);
		figure.setRadius(radius);
	}

	/**
	 * Returns the ball's position.
	 * @return a Vector which represents the ball's position.
	 */
	public Vector getPosition(){
		return new Vector(figure.getCenterX(), figure.getCenterY());
	}

	/**
	 * Sets the speed's direction of the ball.
	 * The direction is normalized before storing.
	 * @param _speed A vector which represents the speed's direction,
	 *               so the norm is not important.
	 */
	public void setSpeed(Vector _speed){
		speed = _speed.normalize(); //normalizes the speed.
	}


	/**
	 * Returns the speed's direction of the ball.
	 * @return a Vector which represents the speed direction of the ball.
	 */
	public Vector getSpeed(){
		return speed;
	}

	/**
	 * Returns the ball's radius.
	 * @return the ball's radius.
	 */
	public double getRadius(){
		return radius;
	}


}
