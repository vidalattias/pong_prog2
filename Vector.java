/**
 * This class represents a two dimensionnal vector.
 */
public class Vector{
	private double x;
	private double y;

	/**
	 * This constructor builds a new Vector from two components.
	 * @param _x The x component.
	 * @param _y The y component.
	 */
	public Vector(double _x, double _y){
		x = _x;
		y = _y;
	}


	/**
	 * Returns the X component.
	 * @return the Y component.
	 */
	public double getX(){
		return x;
	}

	/**
	 * Returns the Y component.
	 * @return the Y component.
	 */
	public double getY(){
		return y;
	}

	/**
	 * Normalizes the vector.
	 * @return returns a new normalized vector.
	 */
	public Vector normalize(){
		double norm = Math.sqrt(x*x + y*y);
		return new Vector(x/norm, y/norm);

	}
}
