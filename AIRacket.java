import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import javafx.animation.KeyValue;



/**
 * This class extends AbstractRacket and represents a Racket which is controlled by an AI.
 */
public class AIRacket extends AbstractRacket{
	private double racketSpeed;
	private Timeline timeline; //A timeline to animate the racket.
	private double YTarget; //Stores the Y position where the racket should go.

	/**
	 * The constructor builds a player racket from two parameters.
	 * @param _fieldWidth This parameter is the width of the game field.
	 * @param _fieldHeight This parameter is the height of the game field.
	 */
	public AIRacket(double _fieldWidth, double _fieldHeight){
		super(_fieldWidth, _fieldHeight);
		YTarget = _fieldHeight/2;
		racketSpeed = 1000;
		timeline = new Timeline();

		//When a moved is finished, a next move is computed.
		timeline.setOnFinished(event ->{
			nextMove();
		});

		//Starts the animation of the racket.
		timeline.playFromStart();

	}

	/**
	 * When the racket receives new informations from the ball,
	 * A new target position is computed. The animation is stopped.
	 * Then a new animation is started to make the racket goes to the target position.
	 * @param nextYBall The Y component of the position where the ball shall hit.
	 */
	@Override
	public void receivesInformation(double nextYBall){
		YTarget = nextYBall;
		if(timeline.getStatus() == Animation.Status.STOPPED){

			nextMove();
		}
		else{
			//The timeline is stopped and cleared before starting the next animation.
			timeline.stop();
			timeline.getKeyFrames().clear();
			nextMove();

		}
	}

	/**
	 * This method is called when the timeline is stopped.
	 * It computes new KeyFrames and restarts the animation.
	 */
	private void nextMove(){
		//Computes where the racket should go
		double nextY = computesLegalYPosition(YTarget - racketHeight/2);
		//Computes the time needed based on the target's position and the racket's speed.
		double time = Math.abs(getPosition().getY()-nextY)/racketSpeed;
		// time < 0.001 means the racket is already at the target position.
		if (time > 0.001) {
			Duration currentTime = timeline.getCurrentTime();
			Duration nextTime = currentTime.add(Duration.seconds(time));
			KeyValue nextFrameY = new KeyValue(display().yProperty(),
					computesLegalYPosition(nextY));

			timeline.getKeyFrames().add(new KeyFrame(nextTime, nextFrameY));
			timeline.playFrom(currentTime);
		}
	}


}
