# PROG2: Pong Project
Vidal Attias - Valentin Taillandier

 You can compile the code and launch a demo by launching demo.sh. Four kind of parties are shown: 
 1. keyboard (Z,S) vs keyboard (UP, DOWN)
 2. AI vs Mouse
 3. Mouse vs AI
 4. AI vs AI

You can launch the game by yourself with:

    java -jar pong_attias_taillandier.jar playerLeft playerRight
playerLeft and playerRight can be "ai" "mouse" or "keyboard"




